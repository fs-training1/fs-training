import {Button, Col, Row} from 'react-bootstrap'

export default function Profle({userData}){

    const {firstName, lastName, email, mobileNo, isAdmin} = userData;

    return(
    (isAdmin)
    ?
    <Row>
            <Col className="m-5 text-center p-5">
            <h1>Welcome, Admin {firstName}!</h1>
            <Button>Go to Dashboard</Button>
            </Col>
    </Row>
    :
    <Row>
        <Col className="m-5 text-center p-5">
            <h1>Welcome, {firstName} {lastName}!</h1>
            <h4>Contact:</h4>
            <h6>email: {email} | Mobile No: {mobileNo}</h6>
            <Button>Edit Profile</Button>
        </Col>
    </Row>
    )
}