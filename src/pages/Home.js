import Banner from '../components/Banner';

export default function Home(){

    //props for Banner

    let bannerProp ={
        title: "Zuitt Coding Bootcamp",
        content: "Opportunities for everyone, everywhere!",
        label: "enroll"
    }

    return(
        <Banner data = {bannerProp}/>
    )
}