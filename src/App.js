import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Profle from './pages/Profile';


function App() {

  let appProp = {
    firstName: "John",
    lastName: "Doe",
    email: "john.doe@mail.com",
    mobileNo: "09123456789",
    isAdmin: false
  }

  return (
    <>
      <AppNavbar/>
      {/* <Home/> */}
      <Profle userData={appProp}/>
    </>
  );
}

export default App;
