import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

/* 

[SECTION] JSX (JavaScript + XML) --> XML -> Extensible Mark-up Language

--> is an extension of JS that let us create objects which then compiled and added as an HTML elemets.
--> with JSX, we are able to create HTML elements using JS.

--> JSX provides "syntactic sugar" for creating react components.
-->syntax within a programming language that is designed to make things easier to read and or to express.
--> <Home />


*/

/* const name = "Romenick Garcia"
const element = <h1>Hello, {name}!</h1>

root.render(element); */