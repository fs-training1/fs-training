import {Navbar, Container, Nav} from 'react-bootstrap'

export default function AppNavbar(){
    return(
        <Navbar bg="light" expand="lg" fluid>
        <Container>
			<Navbar.Brand href="#home">Zuitt</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				{/*className is use instead class, to specify a CSS class*/}
				<Nav className="me-auto">
					<Nav.Link href="#home">Home</Nav.Link>
					<Nav.Link href="#courses">Courses</Nav.Link>
					<Nav.Link href="/profile">Profile</Nav.Link>
				</Nav>
			</Navbar.Collapse>
            </Container>
		</Navbar>
    )
}