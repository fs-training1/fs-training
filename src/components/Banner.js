import {Button, Col, Row} from 'react-bootstrap'

/* 

Pass-by-value (parameter passing)

Pass-by-reference (object, array)



*/

export default function Banner({data}){

    const {title, content, label} = data;

    return(
    <Row>
            <Col className="m-5 text-center p-5">
                <h1>{title}</h1>
                <p>{content}</p>
                <Button>{label}</Button>
            </Col>
        </Row>
    )
}